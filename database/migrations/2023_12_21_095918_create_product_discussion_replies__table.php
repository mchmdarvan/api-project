<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_discussion_replies', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->foreignUuid('product_discussion_id');
            $table->foreign('product_discussion_id')
                ->references('id')
                ->on('product_discussion')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreignUuid('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->longText('comment');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_discussion_replies_');
    }
};
