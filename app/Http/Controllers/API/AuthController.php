<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

// Dependency for JWT
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;

// Model
use App\Models\User;
use Carbon\Carbon;
use Spatie\Permission\Models\Role;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $credentials = request(['username', 'password']);
        $email_exist = User::where('username', $credentials['username'])->first();

        if (!$email_exist || !$token = JWTAuth::attempt($credentials)) {
            return response()->json(['success' => false, 'message' => 'Email atau password tidak ditemukan'], 404);
        }

        return response()->json([
            'status' => 'Success',
            'message' => 'Login Berhasil',
            'bearer_token' => $token,
            'expired_time' => Carbon::now()->addHour(6)->format('Y-m-d H:i:s')
        ], 201);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'username' => 'required|string|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }
        $exists = User::where('email', $request->email)->first();
        if (!$exists) {
            $user = User::create([
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'username' => $request->get('username'),
                'password' => Hash::make($request->get('password')),
            ]);

            $token = JWTAuth::fromUser($user);

            return response()->json([
                'status' => 'Success',
                'token' => $token
            ], 201);
        } else {
            return response()->json([
                'Status' => false,
                'Message' => 'Email already Register',
            ], 400);
        }
    }

    public function checkToken()
    {
        try {

            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['success' => false, 'message' => "User Not Found"], 404);
            }

            return response()->json(['success' => true, 'message' => "Token Valid"]);
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['success' => false, 'message' => 'Token is Expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['success' => false, 'message' => 'Token Invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['success' => false, 'message' => 'Token Absent'], $e->getStatusCode());
        }
    }

    public function getAuthenticatedUser()
    {
        try {

            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

            $user->getPermissionNames();
            $user->getRoleNames();
            return response()->json(['success' => true, 'message' => 'Profile User', 'data' => $user]);
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());
        }
    }

    public function profile()
    {
        $user = Auth::user();
        if (!$user) {
            return response()->json([
                'Message' => 'You have to login first'
            ], 401);
        } else {
            return response()->json($user, 200);
        }
    }

    public function update(Request $request)
    {
        $data = $request->all();

        $item = Auth::user();

        $item->update($data);

        return response()->json($item);
    }

    public function logout()
    {
        //remove token
        $removeToken = JWTAuth::invalidate(JWTAuth::getToken());

        if ($removeToken) {
            //return response JSON
            return response()->json([
                'success' => true,
                'message' => 'Logout Berhasil!',
            ]);
        }
    }
}
